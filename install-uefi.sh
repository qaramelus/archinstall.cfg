#!/bin/bash

USER=txauma

[[ -z $PASSWD ]] && echo "No password given. Set the PASSWD env variable before continuing!" && exit -1

function basic_system_setup() {
    ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
    hwclock --systohc
    sed -i '177s/#//' /etc/locale.gen
    locale-gen
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf
    echo "arch" >> /etc/hostname
    echo "127.0.0.1 localhost" >> /etc/hosts
    echo "::1       localhost" >> /etc/hosts
    echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
    echo root:$PASSWD | chpasswd
}
# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

function install_basic_system() {
    pacman --noconfirm -S grub efibootmgr networkmanager \
        mtools dosfstools gvfs gvfs-smb nfs-utils ntfs-3g \
        reflector base-devel fakeroot binutils linux-headers \
        acpi acpid avahi bluez bluez-utils cups pulseaudio alsa-utils \
        openssh rsync curl wget dnsutils firewalld \
        terminus-font
}

function install_dev_packages() {
    echo "Installing dev packages..."
    # pacman -S docker
    # nice utils
    # pacman -S gotop figlet
}

function install_yay() {
    su $USER -c "mkdir -p  /tmp/yay && cd /tmp/yay && \
    curl -OJ 'https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=yay' && \
    makepkg -si && rm -rf /tmp/yay"
}

function install_amducode() {
    ## Only for AMD gpu
    pacman -S --noconfirm xf86-video-amdgpu
    sed -i 's|MODULES=()|MODULES=(amdgpu)|' /etc/mkinitcpio.conf
    mkinitcpio -p linux

}

function install_grub() {
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
    sed -i 's|_TIMEOUT.*$|_TIMEOUT=1|' /etc/default/grub
    sed -i 's| quiet"| quiet video=1920x1080"|' /etc/default/grub
    sed -i 's|_GFXMODE=auto|_GFXMODE=1920x1080|' /etc/detault/grub
    grub-mkconfig -o /boot/grub/grub.cfg
}

function install_systemd_services() {
    systemctl enable NetworkManager
    systemctl enable bluetooth
    systemctl enable cups.service
    systemctl enable sshd
    systemctl enable avahi-daemon
    systemctl enable reflector.timer
    systemctl enable fstrim.timer
    systemctl enable firewalld
    systemctl enable acpid
}

function create_user() {
    useradd -m $USER
    echo $USER:$PASSWD | chpasswd
    #usermod -aG docker $USER
    echo "$USER ALL=(ALL) ALL NOPASSWD: ALL" >> /etc/sudoers.d/$USER
}

function install_all {
    basic_system_setup
    install_basic_system
    install_amducode
    install_grub
    install_systemd_services
#    create_user
#    install_yay
}

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"




